package otel.emanuel.lab5.ex2;
public class ProxyImage implements Image{
 
   private boolean rotated;
   private RotatedImage rotatedImage;
   private RealImage realImage;
   private String fileName;
 
   public ProxyImage(String fileName,boolean rotated){
      this.fileName = fileName;
      this.rotated=rotated;
   }
 
   @Override
   public void display() 
   {
      if(rotated==false)
      {
         realImage = new RealImage(fileName);
         realImage.display();
      }
      else
      {
          rotatedImage=new RotatedImage(fileName);
          rotatedImage.display();
      }
   }
}
