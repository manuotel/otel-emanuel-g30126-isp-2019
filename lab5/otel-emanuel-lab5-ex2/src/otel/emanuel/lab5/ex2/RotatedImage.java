package otel.emanuel.lab5.ex2;
public class RotatedImage implements Image
{
    private String fileName;
    public RotatedImage(String fileName)
    {
      this.fileName = fileName;
      rotating(fileName);
   }
    public void display() {
      System.out.println("Displaying rotated " + fileName);
   }
    private void rotating(String fileName){
      System.out.println("Rotating " + fileName);
   }
}
