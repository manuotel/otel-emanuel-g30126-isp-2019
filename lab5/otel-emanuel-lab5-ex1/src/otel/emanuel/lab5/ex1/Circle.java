package otel.emanuel.lab5.ex1;
public class Circle extends Shape
{
    double radius;
    Circle()
    {
        
    }
    Circle(double radius)
    {
        this.radius=radius;
    }
    Circle(double radius,String color,boolean filled)
    {
        this.color=color;
        this.filled=filled;
        this.radius=radius;
    }
    double getRadius()
    {
        return radius;
    }
    void setRadius(double radius)
    {
        this.radius=radius;
    }
    double getArea()
    {
        return 2*3.14*radius*radius;
    }
    double getPerimetre()
    {
        return 2*3.14*radius;
    }
    public String toString()
    {
        return this.color+" "+this.filled+" "+this.radius;
    }
}
