package otel.emanuel.lab5.ex1;
public abstract class Shape
{
    String color;
    boolean filled;
    Shape()
    {
        
    }
    Shape(String color,boolean filled)
    {
        this.color=color;
        this.filled=filled;
    }
    String getColor()
    {
        return color;
    }
    boolean isFilled()
    {
        return filled;
    }
    void setFilled(boolean filled)
    {
        this.filled=filled;
    }
    abstract double getArea();
    abstract double getPerimetre();
    public String toString()
    {
        return color+" "+filled;
    }
}
