package otel.emanuel.lab5.ex3;
public abstract class Sensor 
{
    String location;
    abstract int readValue();
    String getLocation()
    {
        return location;
    }
}
