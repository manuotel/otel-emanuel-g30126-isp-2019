package otel.emanuel.lab5.ex3;
import java.util.Random;
public class LightSensor extends Sensor
{
    int value;
    LightSensor()
    {
        value=0;
    }
    int readValue()
    {
        Random rand=new Random();
        this.value=rand.nextInt(100);
        return value;
    }
}
