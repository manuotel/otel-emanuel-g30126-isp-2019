package otel.emanuel.lab5.ex3;
import java.util.Random;
public class TemperatureSensor extends Sensor
{
    int value;
    TemperatureSensor()
    {
        value=0;
    }
    int readValue()
    {
        Random rand=new Random();
        this.value=rand.nextInt(100);
        return value;
    }
}
