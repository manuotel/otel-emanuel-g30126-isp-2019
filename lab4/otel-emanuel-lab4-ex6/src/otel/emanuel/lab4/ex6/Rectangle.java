package otel.emanuel.lab4.ex6;
public class Rectangle extends Shape
{
    public double width;
    public double lenght;
    Rectangle()
    {
        width=1;
        lenght=1;
    }
    Rectangle(double width,double lenght)
    {
        this.width=width;
        this.lenght=lenght;
    }
    Rectangle(double width,double lenght,String color,boolean filled)
    {
        this.color=color;
        this.filled=filled;
        this.lenght=lenght;
        this.width=width;
    }
    public double getWidth()
    {
        return width;
    }
    public double getLenght()
    {
        return lenght;
    }
    public double getArea()
    {
        return width*lenght;
    }
    public double getPerimetre()
    {
        return width+lenght;
    }
    public void setWidth(double width)
    {
        this.width=width;
    }
    public void setLenght(double lenght)
    {
        this.lenght=lenght;
    }
    public String toString()
    {
        return color+" - "+filled+" - "+width+" - "+lenght;
    }
}
