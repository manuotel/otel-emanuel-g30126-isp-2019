package otel.emanuel.lab4.ex6;
public class Circle extends Shape
{
    public double radius;
    Circle()
    {
        radius=1.0;
    }
    Circle(double radius)
    {
        this.radius=radius;
    }
    Circle(double radius,String color,boolean filled)
    {
        this.radius=radius;
        this.color=color;
        this.filled=filled;
    }
    public double getRadius()
    {
        return radius;
    }
    public void setRadius(double radius)
    {
        this.radius=radius;
    }
    public double getArea()
    {
        return 3.14*radius*radius;
    }
    public double getPerimeter()
    {
        return 2*3.14*radius;
    }
    public String toString()
    {
        return color+" - "+filled+" - "+radius;
    }
}
