package otel.emanuel.lab4.ex1;
public class Circle 
{
    private double radius;
    public String color;
    Circle()
    {
        color="red";
        radius=1.0;
    }
    Circle(double radius)
    {
        this.radius=radius;
    }
    public double getRadius()
    {
        return radius;
    }
    public double getArea()
    {
        return 3.14*radius*radius;
    }
}
