package otel.emanuel.lab4.ex5;
public class Circle 
{
    public double radius;
    public String color;
    Circle()
    {
        color="red";
        radius=1.0;
    }
    Circle(double radius)
    {
        this.radius=radius;
    }
    public double getRadius()
    {
        return radius;
    }
    public double getArea()
    {
        return 3.14*radius*radius;
    }
}
