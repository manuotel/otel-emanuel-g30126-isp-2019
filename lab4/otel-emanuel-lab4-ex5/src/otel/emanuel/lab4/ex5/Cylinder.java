package otel.emanuel.lab4.ex5;
public class Cylinder extends Circle
{
    public double height;
    Cylinder()
    {
        height=1;
    }
    Cylinder(double radius)
    {
        this.radius=radius;
    }
    Cylinder(double radius,double height)
    {
        this.height=height;
        this.radius=radius;
    }
    public double getHeight()
    {
        return height;
    }
    public double getVolume()
    {
        return height*3.14*radius*radius;
    }
}
