package otel.emanuel.lab4.ex4;
public class Book 
{
    public String name;
    public Author[] authors;
    public double price;
    public int qtyInStock;
    Book(String name,Author[] authors,double price)
    {
        this.authors=authors;
        this.name=name;
        this.price=price;
        this.qtyInStock=0;
    }
    Book(String name,Author[] authors,double price,int qtyInStock)
    {
        this.authors=authors;
        this.name=name;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }
    public String getName()
    {
        return name;
    }
    public Author[] getAuthors()
    {
        return authors;
    }
    public double getPrice()
    {
        return price;
    }
    public void setPrice(double price)
    {
        this.price=price;
    }
    public int getQtyInStock()
    {
        return qtyInStock;
    }
    public void setQtyInStock(int n)
    {
        qtyInStock=n;
    }
    public void printAuthors()
    {
        for(int i=1;i<authors.length;i++)
        {
            System.out.println(authors[i].toString());
        }
    }
}
