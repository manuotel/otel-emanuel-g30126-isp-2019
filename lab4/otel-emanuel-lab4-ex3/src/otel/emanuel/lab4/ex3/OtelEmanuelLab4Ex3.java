package otel.emanuel.lab4.ex3;
public class OtelEmanuelLab4Ex3 
{
    public static void main(String[] args) 
    {
        Author a=new Author("a","a",'a');
        Book b=new Book("b",a,100.0);
        System.out.println(b.getAuthor().toString());
        System.out.println(b.getName());
        System.out.println(b.getPrice());
        System.out.println(b.getQtyInStock());
    }
}
