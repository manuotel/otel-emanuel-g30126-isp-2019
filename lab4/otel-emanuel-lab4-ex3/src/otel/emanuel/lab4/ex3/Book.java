package otel.emanuel.lab4.ex3;
public class Book 
{
    public String name;
    public Author author;
    public double price;
    public int qtyInStock;
    Book(String name,Author author,double price)
    {
        this.author=author;
        this.name=name;
        this.price=price;
        this.qtyInStock=0;
    }
    Book(String name,Author author,double price,int qtyInStock)
    {
        this.author=author;
        this.name=name;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }
    public String getName()
    {
        return name;
    }
    public Author getAuthor()
    {
        return author;
    }
    public double getPrice()
    {
        return price;
    }
    public void setPrice(double price)
    {
        this.price=price;
    }
    public int getQtyInStock()
    {
        return qtyInStock;
    }
    public void setQtyInStock(int n)
    {
        qtyInStock=n;
    }
}
