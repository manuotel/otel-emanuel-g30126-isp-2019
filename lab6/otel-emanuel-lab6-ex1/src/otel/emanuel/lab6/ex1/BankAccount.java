package otel.emanuel.lab6.ex1;
public class BankAccount 
{
    String owner;
    double balance;
    BankAccount()
    {
        balance=0;
        owner="";
    }
    BankAccount(String owner,double balance)
    {
        this.balance=balance;
        this.owner=owner;
    }
    public boolean equals(Object obj) 
    {
	if(obj instanceof BankAccount)
        {
            BankAccount o = (BankAccount)obj;
            return  owner==o.owner;
	}
	return false;
    }
    public int hashCode()
    {
        return (int) balance+owner.hashCode();
    }
    public void withdraw(double amount)
    {
        balance=balance-amount;
    }
    public void deposit(double amount)
    {
        balance=balance+amount;
    }
}
