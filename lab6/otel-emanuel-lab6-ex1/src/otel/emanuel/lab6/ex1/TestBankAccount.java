package otel.emanuel.lab6.ex1;
public class TestBankAccount 
{
    public static void main(String[] args) 
    {
        BankAccount a=new BankAccount();
        BankAccount b=new BankAccount("Test1",100);
        BankAccount c=new BankAccount("Test1",200);
        BankAccount d=new BankAccount("Test2",500);
        if(a.equals(b))
        {
            System.out.println("A equals C");
        }
        if(b.equals(c))
        {
            System.out.println("B equals C");
        }
        if(a.equals(d))
        {
            System.out.println("A equals D");
        }
        System.out.println(b.hashCode());
        System.out.println(a.hashCode());
    }
}
