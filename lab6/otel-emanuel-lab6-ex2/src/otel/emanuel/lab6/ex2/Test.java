package otel.emanuel.lab6.ex2;
public class Test
{
    public static void main(String[] args) 
    {
        Bank a=new Bank();
        a.addAccount("Test1", 150);
        a.addAccount("Test2", 120);
        a.addAccount("Test3", 130);
        a.printAccounts();
        a.printAccounts(100,131);
        BankAccount d=a.getAccount("Test2");
        System.out.println(d.owner+" -- "+d.balance);
    }
}
