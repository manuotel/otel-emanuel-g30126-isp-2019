package otel.emanuel.lab6.ex2;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
public class Bank 
{
    ArrayList<BankAccount> l=new ArrayList<>();
    void addAccount(String owner,double balance)
    {
        BankAccount b=new BankAccount(owner,balance);
        l.add(b);
    }
    void printAccounts()
    {
        Collections.sort(l, BankAccount.BnkAccBal);
	for(BankAccount str: l)
        {
            System.out.println(str.owner+" - "+str.balance);
	}
    }
    void printAccounts(double MinBalance,double MaxBalance)
    {
        Collections.sort(l, BankAccount.BnkAccBal);
	for(BankAccount str: l)
        {
            if(str.balance>=MinBalance&&str.balance<=MaxBalance)
            System.out.println(str.owner+" --- "+str.balance);
	}
    }
    BankAccount getAccount(String owner)
    {
        Collections.sort(l, BankAccount.BnkAccBal);
	for(BankAccount str: l)
        {
            if(str.owner==owner)
            return str;
	}
        return null;
    }
}
