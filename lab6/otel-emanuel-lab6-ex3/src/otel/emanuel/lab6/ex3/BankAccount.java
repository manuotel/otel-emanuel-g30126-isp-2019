package otel.emanuel.lab6.ex3;
import java.util.Comparator;
public class BankAccount
{
    String owner;
    double balance;
    BankAccount()
    {
        balance=0;
        owner="";
    }
    BankAccount(String owner,double balance)
    {
        this.balance=balance;
        this.owner=owner;
    }
    public boolean equals(Object obj) 
    {
	if(obj instanceof BankAccount)
        {
            BankAccount o = (BankAccount)obj;
            return  owner==o.owner;
	}
	return false;
    }
    public int hashCode()
    {
        return (int) balance+owner.hashCode();
    }
    public void withdraw(double amount)
    {
        balance=balance-amount;
    }
    public void deposit(double amount)
    {
        balance=balance+amount;
    }
    public int compareTo(Object o) 
    {
            BankAccount p = (BankAccount)o;
            if(balance>p.balance) return 1;
            if(balance==p.balance) return 0;
            return -1; 
     }
    public static Comparator<BankAccount> BnkAccBal = new Comparator<BankAccount>() 
    {
	public int compare(BankAccount b1, BankAccount b2) 
        {
	   return (int) (b1.balance-b2.balance);
        }
    };
}
