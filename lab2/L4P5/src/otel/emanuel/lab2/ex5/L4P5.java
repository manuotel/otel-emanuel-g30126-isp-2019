package otel.emanuel.lab2.ex5;
import java.util.Random;
public class L4P5 
{
    public static int getRandom()
    {
        Random t=new Random();
        int i=t.nextInt(100);
        return i;
    }
    public static void main(String[] args) 
    {
        int[] x=new int[20];
        int i,j;
        for(i=1;i<=10;i++)
        {
            x[i]=getRandom();
        }
        for(i=1;i<=10;i++)
        {
            for(j=i+1;j<=10;j++)
            {
                if(x[i]<x[j])
                {
                    int aux;
                    aux=x[i];
                    x[i]=x[j];
                    x[j]=aux;
                }
            }
        }
        for(i=1;i<=10;i++)
        {
            System.out.println(x[i]);
        }
    }
    
}
