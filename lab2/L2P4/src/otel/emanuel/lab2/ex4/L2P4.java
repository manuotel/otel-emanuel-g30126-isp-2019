package otel.emanuel.lab2.ex4;
import java.util.Scanner;
import java.util.Random;
public class L2P4 
{
    static int getRandom()
    {
        Random t=new Random();
        int i=t.nextInt(100);
        return i;
    }
    public static void main(String[] args) 
    {
        int max=0;
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        int[] x=new int[20];
        for(int i=1;i<=n;i++)
        {
            x[i]=getRandom();
            if(i==1)
            {
                max=x[i];
            }
            else
            {
                if(max<x[i])
                {
                    max=x[i];
                }
            }
        }
        System.out.println(max);
    }
    
}
