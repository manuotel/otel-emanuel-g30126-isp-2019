package otel.emanuel.lab2.ex6;
import java.util.Scanner;
public class L2P6 
{
    public static void factorial(int result,int n)
    {
        if(n==0)
        {
            System.out.println(result);
        }
        else
        {
            factorial(result*n,n-1);
        }
    }
    public static void main(String[] args) 
    {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        int result=1,i;
        if(n==0)
            System.out.println(result);
        else
            for(i=1;i<=n;i++)
            {
                result=result*i;
            }
        System.out.println(result);
        result=1;
        factorial(result,n);
    }
}
