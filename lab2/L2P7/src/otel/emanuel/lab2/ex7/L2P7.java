package otel.emanuel.lab2.ex7;
import java.util.Scanner;
import java.util.Random;
public class L2P7 
{
    static int getRandom()
    {
        Random t=new Random();
        int i=t.nextInt(10);
        return i;
    }
    public static void main(String[] args) 
    {
        Scanner in=new Scanner(System.in);
        int n=0;
        int c=getRandom();
        int sanse=3;
        while(sanse!=0)
        {
            n=in.nextInt();
            if(n==c)
            {
                System.out.println("Felicitari! "+c);
                break;
            }
            if(n<c)
            {
                System.out.println("Prea mic!");
            }
            if(n>c)
            {
                System.out.println("Prea mare!");
            }
            sanse--;
        }
        if(n!=c)
        {
            System.out.println("You lost! "+c);
        }
    }
}
