package otel.emanuel.lab12.ex1;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TempSensor extends Thread
{
    int temperature;
    TempSensor()
    {
        Random randomGenerator = new Random();
        int r50 = randomGenerator.nextInt(100) - 50;
        temperature=r50;
    }
    @Override
    public void run()
    {
        NewJFrame a = new NewJFrame();
        while(true)
        {
            Random randomGenerator = new Random();
            int r50 = randomGenerator.nextInt(100) - 50;
            temperature=r50;
            System.out.println(r50);
            a.setLabel(r50);
            try 
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException ex) 
            {
                Logger.getLogger(TempSensor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
