package otel.emanuel.lab7.ex1;
public class PredefinedException extends Exception
{
    int n;
    public PredefinedException(int n,String msg) 
    {
        super(msg);
        this.n = n;
    }
    int getNumber()
    {
        return n;
    }
}
