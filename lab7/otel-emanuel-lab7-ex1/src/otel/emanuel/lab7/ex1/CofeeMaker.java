package otel.emanuel.lab7.ex1;
class CofeeMaker {
    int n=0;
    Cofee makeCofee() throws PredefinedException
    {
            System.out.println("Make a coffe");
            int t = (int)(Math.random()*100);
            int c = (int)(Math.random()*100);
            n++;
            Cofee cofee = new Cofee(t,c);
            if(n>3)
            {
                throw new PredefinedException(n,"Cofee has to many instancies!");
            }
            return cofee;
    }
      int getN()
      {
          return n;
      }
}
