package otel.emanuel.lab3.ex5;
public class Flower
{
    static int number=0;
    int petal;
    Flower()
    { 
        System.out.println("Flower has been created!");
        number++;
    }
    public int retnum()
    {
        return number;
    }
    public static void main(String[] args) 
    {
	Flower[] garden = new Flower[5];
        for(int i=0;i<5;i++)
        {
            Flower f = new Flower();
            garden[i] = f;
        }
        Flower f = new Flower();
        System.out.println(f.retnum());
    }
}