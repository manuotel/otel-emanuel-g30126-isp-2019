package otel.emanuel.lab3.ex3;
public class Author {
    private String name;
    private String email;
    private char gender;
    Author(String name,String email,char gender)
    {
        this.email=email;
        this.gender=gender;
        this.name=name;
    }
    public String getName()
    {
        return name;
    }
    public String getEmail()
    {
        return email;
    }
    public char getGender()
    {
        return gender;
    }
    public String toString()
    {
        return "Author - "+name+"("+gender+") "+"at email "+email;
    }
}
