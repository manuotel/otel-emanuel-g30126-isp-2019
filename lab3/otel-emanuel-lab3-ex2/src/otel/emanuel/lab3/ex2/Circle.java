package otel.emanuel.lab3.ex2;
public class Circle {
    private double value;
    private String color;
    Circle()
    {
        value=1.0;
        color="red";
    }
    Circle(double nvalue,String ncolor)
    {
        value=nvalue;
        color=ncolor;
    }
    public double getValue()
    {
        return value;
    }
    public double getArea()
    {
        return 2*3.14*value*value;
    }
}
