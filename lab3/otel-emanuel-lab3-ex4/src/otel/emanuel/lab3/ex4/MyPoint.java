package otel.emanuel.lab3.ex4;

import static java.lang.Math.sqrt;

public class MyPoint 
{
    int x;
    int y;
    MyPoint()
    {
        x=0;
        y=0;
    }
    MyPoint(int x,int y)
    {
        this.x=x;
        this.y=y;
    }
    public int getx()
    {
        return x;
    }
    public int gety()
    {
        return y;
    }
    public void setx(int x)
    {
        this.x=x;
    }
    public void sety(int y)
    {
        this.y=y;
    }
    public void setxy(int x,int y)
    {
        this.x=x;
        this.y=y;
    }
    public String toString()
    {
        return "("+x+","+y+")";
    }
    public double distance(int x,int y)
    {
        return sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
    }
    public double distance(MyPoint another)
    {
        int ax=another.getx();
        int ay=another.gety();
        return sqrt((this.x-ax)*(this.x-ax)+(this.y-ay)*(this.y-ay));
    }
}
