package aut.utcluj.isp.ex2;

/**
 * @author stefan
 */
public class OnlineShop{
    Shop shop;
    private String webAddress;

    public OnlineShop(String name, String city, String webAddress) {
        this.shop=new Shop(name,city);
        this.webAddress=webAddress;
    }

    public String getWebAddress() {
        return webAddress;
    }
    @Override
    public String toString()
    {
        return "Shop "+this.shop.getName()+" City: "+this.shop.getCity()+" Web address "+this.webAddress;
    }
}
