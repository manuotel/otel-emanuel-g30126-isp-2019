package aut.utcluj.isp.ex3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author stefan
 */
public class StockController {
    /**
     * Add product to catalogue
     *
      Product product  - product information
     * @param quantity - number of times the product should be added
     * @apiNote: if products with the same products id already exists, assume that @param product has the same data
     */
    
    ArrayList<Product>catalgue = new ArrayList<Product>();
    int quantity;
    
    public void addProductToCatalogue(final Product product, final int quantity) {
        for(int i=0;i<quantity;i++)
            this.catalgue.add(product);
    }

    /**
     * Return catalogue information
     *
     * @return dictionary where the key is the product id and the value is an array of products with the same id
     */
    public Map<String, List<Product>> getCatalogue() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Return all the products with particular id
     *
     * @param productId - unique product id
     * @return - list of existing products with same id or null if not found
     */
    public List<Product> getProductsWithSameId(final String productId) {
        ArrayList<Product>temp = new ArrayList<Product>();
        for (Product obj : this.catalgue) {
            if(obj.getId()==productId)
            {
                temp.add(obj);
            }
        }
        if(temp.size()==0)
        {
            return null;
        }
        else
        {
            return temp;
        }
    }

    /**
     * Get total number of products from catalogue
     *
     * @return
     */
    public int getTotalNumberOfProducts() {
        return this.catalgue.size();
    }

    /**
     * Remove all products with same product id
     *
     * @param productId - unique product id
     * @return true if at least one product was deleted or false instead
     */
    public boolean removeAllProductsWitProductId(final String productId) {
        boolean condition=false;
        int i=0;
        for (Product obj : this.catalgue) {
            i++;
            if(obj.getId()==productId)
            {
                this.catalgue.remove(i);
                return condition;
            }
        }
        return condition;
    }

    /**
     * Update the price for all the products with same product id
     *
     * @param productId - unique product id
     * @param price     - new price to be added
     * @return true if at least one product was updated or false instead
     */
    public boolean updateProductPriceByProductId(final String productId, final Double price) {
        boolean condition=false;
        for (Product obj : this.catalgue) {
            if(obj.getId()==productId)
            {
                obj.setPrice(price);
                condition=true;
            }
        }
        return condition;
    }
}
