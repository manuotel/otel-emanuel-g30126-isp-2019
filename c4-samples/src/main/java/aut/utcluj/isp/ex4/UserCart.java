package aut.utcluj.isp.ex4;

import java.util.ArrayList;
import java.util.List;

/**
 * @author stefan
 */
public class UserCart {
    private List<Product> cardProducts;
    private double totalPrice;

    public double getTotalPrice() {
        return totalPrice;
    }

    public List<Product> getCardProducts() {
        return cardProducts;
    }
    public UserCart()
    {
        cardProducts=new ArrayList<>();
        totalPrice=0;
    }

    /**
     * Add new product to user cart
     *
     * @param product  - product to be added
     * @param quantity - number of products of the same type to be added
     */
    public void addProductToCart(final Product product, int quantity) {
        for(int i=0;i<quantity;i++)
            this.cardProducts.add(product);
        this.totalPrice+=product.getPrice()*quantity;
    }

    /**
     * Remove one product with product id from cart
     * If the product with desired id not found in the card, an {@link ProductNotFoundException} exception will be thrown
     *
     * @param productId - unique product id
     */
    public void removeProductFromCart(final String productId) {
        int i=0;
        for (Product obj : this.cardProducts) {
            i++;
            if(obj.getProductId()==productId)
            {
                this.cardProducts.remove(i);
            }
        }
    }

    /**
     * Reset user cart
     * Reset products and total price to default values
     */
    public void resetCart() {
        this.cardProducts=new ArrayList<>();
        this.totalPrice=0;
    }
}
